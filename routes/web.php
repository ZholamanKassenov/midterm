<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/choco', function () {
    return view('index');
});

Route::get('/tabagan', function () {
    return view('page2');
});

Route::get('/circus', function () {
    return view('page3');
});

Route::get('page3.blade.php', function () {
    return view('chocolife.page3');
});

Route::get('page2.blade.php', function () {
    return view('chocolife.page2');
});

Route::resource('crud-panel', 'ContrCRUD');
