<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Circus</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/main.css">

</head>
<body>
 
    	 <div class="wraper">
    <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
        </div>
        <!-- Collection of nav links, forms, and other content for toggling -->
        <div class="container">
        	<div class="row">
        		<div class="bs-example">
        		    <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
								<li class="items-choco"><a href="" class="link_main"></a>
								 </li>
								<li class="items-chocomart"><a href="chocomart.kz" title="">.</a></li>
								<li class="items-chocotravel"><a href=""></a></li>
								<li class="items-optic"><a href=""></a></li>
								<li class="items-chocofood"><a href=""></a></li>
								<li class="items-doctor"><a href=""></a></li>
            </ul>
         
            <ul class="nav navbar-nav navbar-right">
                <li><button  class="registration_but type="submit" class="btn btn-default">Регистрация</button></li>
                <li><a class="enter_t" href="">Вход</a>
               
            </ul>
            </div>
        	</div>
        </div>
         </div>
    </nav>
</div>
				
<div class="container">
 <div class="row">
 	<div class="bs-example">
 	  <div class="col-md-2" >
 	  	<div class="btn-group">
  <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Алматы</button>
  <ul class="dropdown-menu">
    <li><a href="#">Астана</a></li>
                     <li class="divider"></li>
    <li><a href="#">Алматы</a></li>
    <li class="divider"></li>
    <li><a href="#">Шымкент</a></li>
                     <li class="divider"></li>
    <li><a href="#">Тараз</a></li>
  </ul>
</div></div>
  <div class="help">
                    <a href="#" class="help__item">
                        <div class="help__icon help__icon--bulb"></div>
                        <p class="help__content">нужна помощь?</p>
                    </a>
                    <a href="#" class="help__item">
                        <div class="help__icon help__icon--shield"></div>
                        <p class="help__content">защита покупателей</p>
                    </a>
                    <a href="#" class="help__item">
                        <div class="help__icon help__icon--message"></div>
                        <p class="help__content">обратная связь</p>
                    </a>
                </div>
    		<div class="logo-wraper">
	<div class="choco-logo">
		<a href="">
			<span class="b-choco-logo"></span>
		</a>
		<p class="logo-text">
			Главное, чтобы Вы
              <br>были счастливы!
		</p>
	</div>
</div>
<div class="search-wrap">
	<div class="b-search">
		<form action="search" class="b-search-plase">
			<input type="search" placeholder="Найти среди 614 акций" class="search-input">
			<button type="submit" class="e-submit"></button>
		</form>
	</div>
</div>
</div>
<div class="banner-block">
	<span class="closer"></span>
	<div class="b-banner">
		<img src="img/banner.jpg" alt="" class="banner">
	</div>
</div>

</div>
</div>
<div class="categories">
        <div class="categoriesitem">Все</div>
        <div class="categoriesitem">Новые</div>
        <div class="categoriesitem red">Хиты продаж</div>
        <div class="categoriesitem">Развлечения и отдых</div>
        <div class="categoriesitem">Красота и здоровье</div>
        <div class="categoriesitem">Спорт</div>
        <div class="categoriesitem">Товары</div>
        <div class="categoriesitem">Услуг</div>
        <div class="categoriesitem">Еда</div>
        <div class="categoriesitem">Туризм, отели</div>
     
    </div>

<div class="container">
	<div class="row">
	<div class="offer-block">
		<div class="offer-top">Можно купить с 25 января по 31 января
            Можно воспользоваться до 4 февраля 2018 года</div>
		<h1 class="title">Цирк! Цирк! Цирк! Артисты «Большого Московского цирка» и «Цирка Никулина» с новой зажигательной программой ждут вас 4 февраля! Билеты со скидкой 30%!</h1>
  <div class="row">

  	  <div class="col-xs-12 col-sm-6 col-md-8">
  	  	<div class="slider">
  	  		<div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
  <!-- Индикаторы для карусели -->
  <ol class="carousel-indicators">
    <!-- Перейти к 0 слайду карусели с помощью соответствующего индекса data-slide-to="0" -->
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <!-- Перейти к 1 слайду карусели с помощью соответствующего индекса data-slide-to="1" -->
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <!-- Перейти к 2 слайду карусели с помощью соответствующего индекса data-slide-to="2" -->
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>   
  <!-- Слайды карусели -->
  <div class="carousel-inner">
    <!-- Слайды создаются с помощью контейнера с классом item, внутри которого помещается содержимое слайда -->
    <div class="active item">
       <div class="carousel-caption">
	  <div class="slid"><a href=""><img src="img/cir1.jpg" alt=""></a></div>
       
      </div>
    </div>
    <!-- Слайд №2 -->
    <div class="item">
      <div class="carousel-caption">
        <div class="slid"><a href=""><img src="img/cir2.jpg" alt=""></a></div>
      </div>
    </div>
    <!-- Слайд №3 -->
    <div class="item">
      <div class="carousel-caption">
        <div class="slid"><a href=""><img src="img/cir1.jpg" alt=""></a></div>
      </div>
    </div>
  </div>
  <!-- Навигация для карусели -->
  <!-- Кнопка, осуществляющая переход на предыдущий слайд с помощью атрибута data-slide="prev" -->
  <a class="carousel-control left" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <!-- Кнопка, осуществляющая переход на следующий слайд с помощью атрибута data-slide="next" -->
  <a class="carousel-control right" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
  	  	</div>
  	  

  	  </div>
  <div class="col-xs-6 col-md-4">
  	
  	<p class="price">от 1 400 тг.</p>
  	<p class="bought_price">Купили 2396 человек</p>
  	<a href="#" class="e-button">
          <span>Купить</span>
    </a>
  </div>
</div>
	</div>

</div>
</div>
<div class="container">
	<div class="row">
	<div class="offer-block">
		<div class="offer-top-2">
			<ul class="nav nav-pills">
  <li class="active">
    <a href="#">Информация</a>
  </li>
  <li><a href="#">Отзывы</a></li>
  <li><a href="#">Вопросы 157</a></li>
      <li><a href="#">Получить 5000 тенге</a></li>
        <li><a href="#">Как воспользоваться акцией?</a></li>
</ul>
		</div>
<div class="row">
  <div class="col-md-6">
  	<div class="information">
  		<a href="/chocopromise#extended" class="extended_defense">
                        <div class="extended_defense_img"></div>
                        <p class="extended_defense_txt">Расширенная защита покупателей</p>
                    </a>
  		<div class="text-inf">
  			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque a vitae, ipsum incidunt quaerat quos laborum ad expedita dolorem quae, molestiae, quam odio nostrum reprehenderit natus sit quasi blanditiis maiores.</p>
  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error maxime laboriosam ea placeat earum rem provident minus iste, consequuntur optio id odit deleniti, doloremque expedita, officia, facere ipsum ducimus non!</p> 
  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, temporibus! Consequuntur harum, dicta eius rem minus modi atque repudiandae dolor sed. Suscipit illo beatae ullam mollitia quam officiis pariatur laboriosam!</p>
  		</div>
  	</div>

  </div>
  <div class="col-md-6">
  	<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/p8W8WsCm-gU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
  	
  	<div class="b-offer__features">
  		<p class="e-offer__features">Особенности:</p>
       <ul class="b-offer__features-list">
       	<li class="e-offer__feature">Артисты «Большого Московского цирка» и «Цирка Никулина» лауреаты и призеры различных мировых фестивалей циркового искусство представляют:</li>
       </ul>
  	</div>


  </div>
</div>
<div class="b-offer__bottom">
	<div class="b-offer__howpay">
                <span class="b-offer__howpay-text">Способы оплаты:</span>
                <span class="b-offer_visa"></span>
                <span class="b-offer_mastercard">&nbsp;</span>
                                    <span class="b-offer_qiwi">&nbsp;</span>
                            </div>
                            <div class="e-offer__button__bottom">
                            	<a href="">Купить</a>
                            </div>

</div>
</div>
	</div>

</div>
</div>

<footer id="myFooter">
        <div class="container">
            <div class="row">
            	   <div class="col-sm-1">
                 
                </div>
                    <div class="col-sm-2">
                    <h5>Компания</h5>
                    <ul>
                        
                        <li><a href="#">О Chocolife.me</a></li>
                        <li><a href="#">Пресса о нас</a></li>
                        <li><a href="#">Контакты</a></li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <h5>Клиентам</h5>
                    <ul>
                       
                        <li><a href="#">Обратная связь</a></li>
                        <li><a href="#">Обучающий видеоролик</a></li>
                        <li><a href="#">Вопросы и ответы</a></li>
                        <li><a href="#">Публичная оферта</a></li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <h5>Партнерам</h5>
                    <ul>
                        
                        <li><a href="#">Для Вашего бизнеса</a></li>
                        <li><a href="#">Наше приложение</a></li>
                    </ul>
                </div>
      
                <div class="col-sm-3">
                    <div class="social-networks">
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                    </div>
                 
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <p>© Chocolife.me | 2011-2018 </p>
        </div>
    </footer>
</body> 
</html>
</body> 
</html>