@extends('CRUD.crud-index')

@section('title', 'Add post')

@section('content')

<div class="col-md-7">
    {!! Form::model($post, array('route' => array('crud-panel.update', $post->id), 'method' => 'PUT')) !!}
    <div class="form-group">
        <div class="col-md-3">
         {{ Form::label('title', 'Header') }}
        </div>
        <div class="col-md-9">
         {{ Form::text('title', null, ['class' => 'form-control'])}}
        </div>
    
    <div class="form-group">
        <div class="col-md-3">
         {{ Form::label('slug', 'URL') }}
        </div>
        <div class="col-md-9">
         {{ Form::text('slug', null, ['class' => 'form-control'])}}
        </div>
        
        <div class="form-group">
        <div class="col-md-3">
         {{ Form::label('content', 'Text') }}
        </div>
        <div class="col-md-9">
         {{ Form::textarea('content', null, ['class' => 'form-control'])}}
        </div>
        
        <div class="form-group">
        <div class="col-md-9 col-md-offset-3">
         {{ Form::submit('Submit', ['class' => 'btn btn-primary'])}}
        </div>
        
            
    </div>
    {!! Form::close() !!}
</div>
@endsection
