@extends('CRUD.crud-index')

@section('title', 'Al posts')

@section('content')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Header</th>
            <th>UML</th>
            <th>Content</th>
            <th style="width: 260px">Control</th>
        </tr>
        </thead>
        <tbody>
     
        @foreach($Posts as $p)
            <tr>
                <th scope="row">{{ $p->id }}</th>
                <td>{{ $p->title }}</td>
                <td>{{ $p->slug }}</td>
                <td>{!! $p->content !!}</td>
                <td>
                    <a href="{{ URL::to('crud-panel/' . $p->id) . '/edit' }}" class="btn btn-default"  style="float: left;margin-right: 10px">Edit</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['crud-panel.destroy', $p->id]]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
   
        </tbody>
    </table>

    {{ $Posts->links() }}

@endsection