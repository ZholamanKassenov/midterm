@extends('CRUD.crud-index')

@section('title', 'Show post')

@section('content')

<h1>{{ $post->title}}</h1>
<p>{{ $post->created_at}}</p>
<p>{!! $post->content !!}</p>

<a href="{{ URL::to('crud-panel/'. $post->id). '/edit'}}" class="btn btn-default">Edit</a>
{!! Form::open(['method' => 'DELETE', 'route' => ['crud-panel.destroy', $post->id]])!!}
{!! Form::submit('Delete', ['class' => 'btn btn-danger'])!!}
{!! Form::close() !!}

@endsection